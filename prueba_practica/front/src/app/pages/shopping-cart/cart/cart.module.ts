import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart.component';
import { MaterialModule } from '../../../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { ProductComponent } from './product/product.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ModalProductsComponent } from './modal-products/modal-products.component';

@NgModule({
  declarations: [CartComponent, ProductComponent, ModalProductsComponent],
  imports: [
    CommonModule,
    CartRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    MatCardModule,
    FlexLayoutModule,
  ],
  exports: [CartComponent],
})
export class CartModule {}
