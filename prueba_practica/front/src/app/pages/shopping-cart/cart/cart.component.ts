import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subject, takeUntil, Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { ProductService } from '../service/product.service';
import { ModalProductsComponent } from './modal-products/modal-products.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  allProducts: any[] = [];

  dataSource = new MatTableDataSource();
  private destroy$ = new Subject<any>();
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private productSvc: ProductService, private _router: Router) {}

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  loadProducts(): void {
    this.productSvc.getProducts().subscribe({
      next: (products) => {
        this.allProducts = products.content;
        // this.dataSource.data = products;
        console.log('Allproducts', this.allProducts);
      },
    });
  }

  ngOnInit(): void {
    this.loadProducts();
  }

  onDeleteProduct(productId: number): void {
    Swal.fire({
      title: '¿Estás seguro?',
      text: 'No podrás revertir el cambio',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, eliminar!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.productSvc
          .deleteProduct(productId)
          .pipe(takeUntil(this.destroy$))
          .subscribe((res) => {
            Swal.fire('¡Eliminado!', res?.['message'], 'success');
            this.loadProducts();
          });
      }
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
}
